"""GAPI Test"""
import logging
from unittest.mock import MagicMock

import pytest
from httplib2 import Http
from oauth2client.service_account import ServiceAccountCredentials

from fw_gdrive import gapi

from .conftest import DEFAULT_DISCOVERY_DOC, FILE_RESPONSE


def test_gapi_client_init(mocker, tmpdir):
    # Create a Mock object for service Account Cred
    service_acc_cred_mock = ServiceAccountCredentials("name@email.com", object())
    cred_http_mock = service_acc_cred_mock.authorize(Http())

    # MagicMock for mock_login return value
    service_acc_cred_magic_mock = MagicMock()
    service_acc_cred_magic_mock.authorize.return_value = cred_http_mock

    # Patch for utils.login and gapi.GoogleAPIClient._get_api_client
    mock_login = mocker.patch("fw_gdrive.gapi.utils.login")
    get_api_client_patch = mocker.patch(
        "fw_gdrive.gapi.GoogleAPIClient._get_api_client"
    )
    # Set return value for utils.login to MagicMock
    mock_login.return_value = service_acc_cred_magic_mock

    # Create dir to host credentials.json
    cred_temp_dir = tmpdir.mkdir("creds")
    cred_temp_dir.join("credentials.json").write("testing-credentials")
    cred_path = cred_temp_dir.join("credentials.json")

    actual_api_client_obj = gapi.GoogleAPIClient(cred_path)

    # Assert
    get_api_client_patch.assert_any_call(
        svc_acc_cred=service_acc_cred_magic_mock, service="drive", version="v3"
    )
    get_api_client_patch.assert_any_call(
        svc_acc_cred=service_acc_cred_magic_mock,
        service="docs",
        version="v1",
        discovery_uri=DEFAULT_DISCOVERY_DOC,
        http=cred_http_mock,
    )
    assert actual_api_client_obj.svc_acc_cred == service_acc_cred_magic_mock


def test_get_api_client(mocker):
    # Patch for discovery.build
    discovery_build_patch = mocker.patch("fw_gdrive.gapi.discovery.build")

    # Create a Mock objects for service Account Cred
    service_acc_cred_mock = ServiceAccountCredentials("name@email.com", object())
    service_acc_cred_mock.authorize(Http())
    service_acc_cred_mock_2 = ServiceAccountCredentials("name_2@email.com", object())

    gapi.GoogleAPIClient._get_api_client(
        svc_acc_cred=service_acc_cred_mock, service="drive", version="v3"
    )
    gapi.GoogleAPIClient._get_api_client(
        svc_acc_cred=service_acc_cred_mock_2,
        service="test",
        version="v00",
        num_retries=1000,
    )

    # Assert
    discovery_build_patch.assert_any_call(
        serviceName="drive",
        version="v3",
        num_retries=7,
        credentials=service_acc_cred_mock,
        discoveryServiceUrl=None,
    )
    discovery_build_patch.assert_any_call(
        serviceName="test",
        version="v00",
        num_retries=1000,
        credentials=service_acc_cred_mock_2,
        discoveryServiceUrl=None,
    )


def test_GoogleDocsFile_init():
    docs_service_mock = MagicMock()
    mock_file_id = "abc"
    docs_file_obj_mock = gapi.GoogleDocsFile(
        file_id=mock_file_id, docs_service=docs_service_mock
    )

    assert docs_file_obj_mock.file_id == mock_file_id
    assert docs_file_obj_mock.docs_service == docs_service_mock


def test_get_file_content(example_doc_content):
    docs_service_mock = MagicMock()
    mock_file_id = "abc"
    docs_file_obj_mock = gapi.GoogleDocsFile(
        file_id=mock_file_id, docs_service=docs_service_mock
    )
    docs_service_mock.documents().get(
        documentId=mock_file_id, fields="body"
    ).execute.return_value = {"body": {"content": example_doc_content}}

    actual_file_content_response = docs_file_obj_mock.get_file_content()
    assert actual_file_content_response == example_doc_content


def test_update_document_request(
    example_replace_text_request, example_doc_style_update_request
):
    docs_service_mock = MagicMock()
    mock_file_id = "abc"
    docs_file_obj_mock = gapi.GoogleDocsFile(
        file_id=mock_file_id, docs_service=docs_service_mock
    )
    docs_service_mock.documents().batchUpdate(
        documentId=mock_file_id, body={}
    ).execute.return_value = {
        "replies": [{"replaceAllText": {}, "updateDocumentStyle": {}}],
        "writeControl": {"requiredRevisionId": "ABCD1234"},
        "documentId": "1234file-asd",
    }

    actual_file_request_result = docs_file_obj_mock.update_document_request(
        [example_replace_text_request, example_doc_style_update_request]
    )
    assert actual_file_request_result == {
        "replies": [{"replaceAllText": {}, "updateDocumentStyle": {}}],
        "writeControl": {"requiredRevisionId": "ABCD1234"},
        "documentId": "1234file-asd",
    }


@pytest.mark.parametrize(
    "p_style,index_range",
    [
        (
            "HEADING_1",
            [{"startIndex": 1, "endIndex": 54}, {"startIndex": 927, "endIndex": 933}],
        ),
        (
            "HEADING_2",
            [
                {"startIndex": 54, "endIndex": 63},
                {"startIndex": 519, "endIndex": 526},
                {"startIndex": 735, "endIndex": 752},
                {"startIndex": 974, "endIndex": 984},
            ],
        ),
        ("NAMED_STYLE_TYPE_UNSPECIFIED", []),
    ],
)
def test_find_index_range_by_paragraph_style(example_doc_content, p_style, index_range):
    docs_service_mock = MagicMock()
    mock_file_id = "abc"
    docs_file_obj_mock = gapi.GoogleDocsFile(
        file_id=mock_file_id, docs_service=docs_service_mock
    )
    docs_service_mock.documents().get(
        documentId=mock_file_id, fields="body"
    ).execute.return_value = {"body": {"content": example_doc_content}}

    actual_range_list = docs_file_obj_mock.find_index_range_by_paragraph_style(p_style)
    assert actual_range_list == index_range


@pytest.mark.parametrize(
    "search_content,index_range",
    [
        ("Inputs\n", [{"startIndex": 519, "endIndex": 526}]),
        ("A DICOM archive (‘.zip’)", [{"startIndex": 593, "endIndex": 631}]),
        (
            "This gear will populate some metadata at some point.\n",
            [{"startIndex": 874, "endIndex": 927}],
        ),
        ("abcsed", []),
    ],
)
def test_find_index_range_by_content(example_doc_content, search_content, index_range):
    docs_service_mock = MagicMock()
    mock_file_id = "abc"
    docs_file_obj_mock = gapi.GoogleDocsFile(
        file_id=mock_file_id, docs_service=docs_service_mock
    )
    docs_service_mock.documents().get(
        documentId=mock_file_id, fields="body"
    ).execute.return_value = {"body": {"content": example_doc_content}}

    actual_range = docs_file_obj_mock.find_index_range_by_content(search_content)

    assert actual_range == index_range


@pytest.mark.parametrize(
    "obj_name,index_range",
    [
        (
            "horizontalRule",
            [
                {"startIndex": 507, "endIndex": 509},
                {"startIndex": 517, "endIndex": 519},
            ],
        ),
        ("autoText", [{"startIndex": 1114, "endIndex": 1200}]),
        ("equation", []),
    ],
)
def test_find_index_range_by_object(example_doc_content, obj_name, index_range):
    docs_service_mock = MagicMock()
    mock_file_id = "abc"
    docs_file_obj_mock = gapi.GoogleDocsFile(
        file_id=mock_file_id, docs_service=docs_service_mock
    )
    docs_service_mock.documents().get(
        documentId=mock_file_id, fields="body"
    ).execute.return_value = {"body": {"content": example_doc_content}}
    actual_range = docs_file_obj_mock.find_index_range_by_paragraph_element(obj_name)

    assert actual_range == index_range


def test_GoogleDriveFolder_init():
    folder_cred_mock = MagicMock()
    mock_folder_id = "FolderABC"
    api_client_mock = MagicMock()
    api_client_mock.drive_service = folder_cred_mock
    drive_obj_mock = gapi.GoogleDriveFolder(
        folder_id=mock_folder_id, api_client=api_client_mock
    )

    assert drive_obj_mock.folder_id == mock_folder_id
    assert drive_obj_mock.folder_cred == folder_cred_mock


def test_upload_file(tmpdir):
    example_folder_id = "FolderABC"

    # Sample info about the file for uploading
    temp_dir = tmpdir.mkdir("docs")
    temp_dir.join("example_text.docx").write("testing")

    folder_cred_mock = MagicMock()
    api_client_mock = MagicMock()
    docs_service_mock = MagicMock()
    drive_obj = gapi.GoogleDriveFolder(
        folder_id=example_folder_id, api_client=api_client_mock
    )
    drive_obj.folder_cred = folder_cred_mock
    drive_obj.api_client.doc_service.return_value = docs_service_mock
    folder_cred_mock.files().create().execute.return_value = {"id": "abc"}

    actual_docs_file_obj = drive_obj.upload_file(
        file_name="fileABC", file_path=temp_dir.join("example_text.docx")
    )

    assert actual_docs_file_obj.file_id == "abc"
    assert isinstance(actual_docs_file_obj, gapi.GoogleDocsFile)


def test_get_file_list_from_folder():
    example_folder_id = "FolderABC"
    api_client_mock = MagicMock()
    folder_cred_mock = MagicMock()

    drive_obj = gapi.GoogleDriveFolder(
        folder_id=example_folder_id, api_client=api_client_mock
    )

    drive_obj.folder_cred = folder_cred_mock
    folder_cred_mock.files().list().execute.return_value = FILE_RESPONSE

    actual_file_list = drive_obj.list_files_in_folder()

    assert actual_file_list == FILE_RESPONSE.get("files")


def test_get_file_id_by_name():
    example_folder_id = "FolderABC"
    api_client_mock = MagicMock()
    folder_cred_mock = MagicMock()

    drive_obj = gapi.GoogleDriveFolder(
        folder_id=example_folder_id, api_client=api_client_mock
    )

    drive_obj.folder_cred = folder_cred_mock
    folder_cred_mock.files().list().execute.return_value = FILE_RESPONSE

    f_list = drive_obj.get_file_id_by_name("random_file_111.txt")
    assert f_list == ["111"]


def test_delete_file_by_id(caplog):
    caplog.set_level(logging.INFO)
    example_folder_id = "FolderABC"
    api_client_mock = MagicMock()
    folder_cred_mock = MagicMock()

    drive_obj = gapi.GoogleDriveFolder(
        folder_id=example_folder_id, api_client=api_client_mock
    )

    drive_obj.folder_cred = folder_cred_mock
    folder_cred_mock.files().delete().execute.return_value = MagicMock()
    success = drive_obj.delete_file_by_id(file_id="111")
    assert success
