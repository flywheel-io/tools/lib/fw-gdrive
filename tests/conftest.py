import json
from pathlib import Path

import pytest

ASSETS_DIR = Path(__file__).parents[0] / "assets"

DEFAULT_DISCOVERY_DOC = "https://docs.googleapis.com/$discovery/rest?version=v1"

FILE_CONTENT_RESPONSE = [
    {
        "endIndex": 1,
        "sectionBreak": {
            "sectionStyle": {
                "columnSeparatorStyle": "NONE",
                "contentDirection": "LEFT_TO_RIGHT",
                "sectionType": "CONTINUOUS",
            }
        },
    },
    {
        "startIndex": 1,
        "endIndex": 2,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 1,
                    "endIndex": 2,
                    "textRun": {"content": "\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "NORMAL_TEXT",
                "lineSpacing": 100,
                "direction": "LEFT_TO_RIGHT",
                "spaceAbove": {"unit": "PT"},
                "spaceBelow": {"unit": "PT"},
                "indentFirstLine": {"magnitude": 36, "unit": "PT"},
            },
        },
    },
    {
        "startIndex": 2,
        "endIndex": 3,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 2,
                    "endIndex": 3,
                    "textRun": {"content": "\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "NORMAL_TEXT",
                "lineSpacing": 100,
                "direction": "LEFT_TO_RIGHT",
                "spaceAbove": {"unit": "PT"},
                "spaceBelow": {"unit": "PT"},
                "indentFirstLine": {"magnitude": 36, "unit": "PT"},
            },
        },
    },
    {
        "startIndex": 3,
        "endIndex": 4,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 3,
                    "endIndex": 4,
                    "textRun": {"content": "\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "NORMAL_TEXT",
                "lineSpacing": 100,
                "direction": "LEFT_TO_RIGHT",
                "spaceAbove": {"unit": "PT"},
                "spaceBelow": {"unit": "PT"},
                "indentFirstLine": {"magnitude": 36, "unit": "PT"},
            },
        },
    },
    {
        "startIndex": 4,
        "endIndex": 5,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 4,
                    "endIndex": 5,
                    "textRun": {"content": "\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "NORMAL_TEXT",
                "lineSpacing": 100,
                "direction": "LEFT_TO_RIGHT",
                "spaceAbove": {"unit": "PT"},
                "spaceBelow": {"unit": "PT"},
                "indentFirstLine": {"magnitude": 36, "unit": "PT"},
            },
        },
    },
    {
        "startIndex": 5,
        "endIndex": 6,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 5,
                    "endIndex": 6,
                    "textRun": {"content": "\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "NORMAL_TEXT",
                "lineSpacing": 100,
                "direction": "LEFT_TO_RIGHT",
                "spaceAbove": {"unit": "PT"},
                "spaceBelow": {"unit": "PT"},
                "indentFirstLine": {"magnitude": 36, "unit": "PT"},
            },
        },
    },
    {
        "startIndex": 6,
        "endIndex": 21,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 6,
                    "endIndex": 21,
                    "textRun": {
                        "content": "Example Readme\n",
                        "textStyle": {
                            "bold": True,
                            "fontSize": {"magnitude": 20, "unit": "PT"},
                        },
                    },
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "NORMAL_TEXT",
                "lineSpacing": 100,
                "direction": "LEFT_TO_RIGHT",
                "spaceAbove": {"unit": "PT"},
                "spaceBelow": {"unit": "PT"},
                "indentFirstLine": {"unit": "PT"},
                "indentStart": {"unit": "PT"},
            },
        },
    },
    {
        "startIndex": 21,
        "endIndex": 22,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 21,
                    "endIndex": 22,
                    "textRun": {"content": "\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "NORMAL_TEXT",
                "lineSpacing": 100,
                "direction": "LEFT_TO_RIGHT",
                "spaceAbove": {"unit": "PT"},
                "spaceBelow": {"unit": "PT"},
            },
        },
    },
    {
        "startIndex": 22,
        "endIndex": 23,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 22,
                    "endIndex": 23,
                    "textRun": {"content": "\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "NORMAL_TEXT",
                "lineSpacing": 100,
                "direction": "LEFT_TO_RIGHT",
                "spaceAbove": {"unit": "PT"},
                "spaceBelow": {"unit": "PT"},
            },
        },
    },
    {
        "startIndex": 23,
        "endIndex": 36,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 23,
                    "endIndex": 36,
                    "textRun": {"content": "Example Gear\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "HEADING_1",
                "direction": "LEFT_TO_RIGHT",
            },
        },
    },
    {
        "startIndex": 36,
        "endIndex": 45,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 36,
                    "endIndex": 45,
                    "textRun": {"content": "Overview\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "HEADING_2",
                "direction": "LEFT_TO_RIGHT",
            },
        },
    },
    {
        "startIndex": 45,
        "endIndex": 53,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 45,
                    "endIndex": 53,
                    "textRun": {"content": "Summary\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "HEADING_3",
                "direction": "LEFT_TO_RIGHT",
            },
        },
    },
    {
        "startIndex": 53,
        "endIndex": 74,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 53,
                    "endIndex": 74,
                    "textRun": {"content": "Just an example here\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "NORMAL_TEXT",
                "direction": "LEFT_TO_RIGHT",
            },
        },
    },
    {
        "startIndex": 74,
        "endIndex": 83,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 74,
                    "endIndex": 83,
                    "textRun": {"content": "Citation\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "HEADING_3",
                "direction": "LEFT_TO_RIGHT",
            },
        },
    },
    {
        "startIndex": 83,
        "endIndex": 96,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 83,
                    "endIndex": 96,
                    "textRun": {"content": "License: MIT\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "NORMAL_TEXT",
                "direction": "LEFT_TO_RIGHT",
            },
        },
    },
    {
        "startIndex": 96,
        "endIndex": 111,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 96,
                    "endIndex": 111,
                    "textRun": {"content": "Classification\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "headingId": "h.yts4kgcahaho",
                "namedStyleType": "HEADING_3",
                "direction": "LEFT_TO_RIGHT",
            },
        },
    },
    {
        "startIndex": 111,
        "endIndex": 124,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 111,
                    "endIndex": 124,
                    "textRun": {"content": "Category: qa\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "NORMAL_TEXT",
                "direction": "LEFT_TO_RIGHT",
            },
        },
    },
    {
        "startIndex": 124,
        "endIndex": 136,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 124,
                    "endIndex": 136,
                    "textRun": {"content": "Gear Level:\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "NORMAL_TEXT",
                "direction": "LEFT_TO_RIGHT",
            },
        },
    },
    {
        "startIndex": 136,
        "endIndex": 146,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 136,
                    "endIndex": 146,
                    "textRun": {"content": "☐ Project\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "NORMAL_TEXT",
                "direction": "LEFT_TO_RIGHT",
                "indentFirstLine": {"magnitude": 12, "unit": "PT"},
                "indentStart": {"magnitude": 36, "unit": "PT"},
            },
            "bullet": {"listId": "kix.list.7", "textStyle": {}},
        },
    },
    {
        "startIndex": 146,
        "endIndex": 156,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 146,
                    "endIndex": 156,
                    "textRun": {"content": "☐ Subject\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "NORMAL_TEXT",
                "direction": "LEFT_TO_RIGHT",
                "indentFirstLine": {"magnitude": 12, "unit": "PT"},
                "indentStart": {"magnitude": 36, "unit": "PT"},
            },
            "bullet": {"listId": "kix.list.7", "textStyle": {}},
        },
    },
    {
        "startIndex": 156,
        "endIndex": 166,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 156,
                    "endIndex": 166,
                    "textRun": {"content": "☒ Session\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "NORMAL_TEXT",
                "direction": "LEFT_TO_RIGHT",
                "indentFirstLine": {"magnitude": 12, "unit": "PT"},
                "indentStart": {"magnitude": 36, "unit": "PT"},
            },
            "bullet": {"listId": "kix.list.7", "textStyle": {}},
        },
    },
    {
        "startIndex": 166,
        "endIndex": 180,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 166,
                    "endIndex": 180,
                    "textRun": {"content": "☐ Acquisition\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "NORMAL_TEXT",
                "direction": "LEFT_TO_RIGHT",
                "indentFirstLine": {"magnitude": 12, "unit": "PT"},
                "indentStart": {"magnitude": 36, "unit": "PT"},
            },
            "bullet": {"listId": "kix.list.7", "textStyle": {}},
        },
    },
    {
        "startIndex": 180,
        "endIndex": 191,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 180,
                    "endIndex": 191,
                    "textRun": {"content": "☐ Analysis\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "NORMAL_TEXT",
                "direction": "LEFT_TO_RIGHT",
                "indentFirstLine": {"magnitude": 12, "unit": "PT"},
                "indentStart": {"magnitude": 36, "unit": "PT"},
            },
            "bullet": {"listId": "kix.list.7", "textStyle": {}},
        },
    },
    {
        "startIndex": 191,
        "endIndex": 192,
        "paragraph": {
            "elements": [
                {
                    "startIndex": 191,
                    "endIndex": 192,
                    "textRun": {"content": "\n", "textStyle": {}},
                }
            ],
            "paragraphStyle": {
                "namedStyleType": "NORMAL_TEXT",
                "lineSpacing": 100,
                "direction": "LEFT_TO_RIGHT",
                "spaceAbove": {"unit": "PT"},
                "spaceBelow": {"unit": "PT"},
                "indentFirstLine": {"magnitude": 36, "unit": "PT"},
                "indentStart": {"unit": "PT"},
            },
        },
    },
]

FILE_RESPONSE = {
    "kind": "drive#fileList",
    "incompleteSearch": False,
    "files": [
        {
            "kind": "drive#file",
            "id": "111",
            "name": "random_file_111.txt",
            "mimeType": "application/vnd.google-apps.document",
            "teamDriveId": "teamdriveabc",
            "driveId": "driveabc",
        },
        {
            "kind": "drive#file",
            "id": "abc",
            "name": "random_file_abc.txt",
            "mimeType": "application/vnd.google-apps.document",
            "teamDriveId": "teamdriveabc",
            "driveId": "driveabc",
        },
        {
            "kind": "drive#file",
            "id": "def",
            "name": "random_file_def.txt",
            "mimeType": "application/vnd.google-apps.document",
            "teamDriveId": "teamdriveabc",
            "driveId": "driveabc",
        },
        {
            "kind": "drive#file",
            "id": "ghi",
            "name": "random_file_ghi.txt",
            "mimeType": "application/vnd.google-apps.document",
            "teamDriveId": "teamdriveabc",
            "driveId": "driveabc",
        },
    ],
}


@pytest.fixture
def paragraph_style():
    def gen(**kwargs):
        default = {
            "namedStyleType": "NORMAL_TEXT",
            "lineSpacing": 100,
            "direction": "LEFT_TO_RIGHT",
            "spaceAbove": {"unit": "PT"},
            "spaceBelow": {"unit": "PT"},
            "indentFirstLine": {"magnitude": 36, "unit": "PT"},
        }
        default.update(kwargs)
        return default

    return gen


@pytest.fixture
def example_range_list():
    def gen(**kwargs):
        default = {"startIndex": 1234, "endIndex": 5678}
        default.update(kwargs)
        return default

    return gen


@pytest.fixture
def example_doc_style():
    def gen(**kwargs):
        default = {"useFirstPageHeaderFooter": True}
        default.update(kwargs)
        return default

    return gen


@pytest.fixture()
def example_doc_content():
    content_path = ASSETS_DIR / "example_file_content.json"

    with open(content_path) as content_file:
        data = json.load(content_file)
    return data


@pytest.fixture
def example_replace_text_request():
    def gen(**kwargs):
        default = {
            "replaceAllText": {
                "containsText": {"text": "dummy text", "matchCase": "true"},
                "replaceText": "another text",
            }
        }
        default.update(kwargs)
        return default

    return gen


@pytest.fixture
def example_doc_style_update_request():
    def gen(**kwargs):
        doc_style_dict = example_doc_style()
        default = {
            "updateDocumentStyle": {
                "documentStyle": doc_style_dict,
                "fields": "useFirstPageHeaderFooter",
            }
        }
        default.update(kwargs)
        return default

    return gen
