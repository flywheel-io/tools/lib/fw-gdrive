"""Utils test"""

import logging
from unittest.mock import call

import pytest

from fw_gdrive import utils

from .conftest import ASSETS_DIR

log = logging.getLogger(__name__)


def test_login(mocker):
    ServiceAccountCredentials = mocker.patch(
        "fw_gdrive.utils.ServiceAccountCredentials"
    )
    service_cred_path = ASSETS_DIR / "dummy_service_creds.json"
    scopes = ["https://www.googleapis.com/auth/drive.file"]

    utils.login(service_cred_path=service_cred_path, scopes=scopes)

    assert ServiceAccountCredentials.mock_calls == [
        call.from_json_keyfile_name(service_cred_path, scopes=scopes),
    ]


def test_parse_error_message():
    test_err_details = [
        {"message": "error abc.", "test": "not this"},
        {"message": "error efg.", "test": "not this v2."},
        {"message": "error hij.", "test 2": "just another test."},
        {"message_": "invalid message.", "test": "not this."},
    ]

    actual_msg = utils.parse_error_message(test_err_details)

    assert actual_msg == "error abc. error efg. error hij."


def test_validate_docs_obj():
    search_obj_arg = "tExTrun"

    mod_search_obj_arg = utils.validate_docs_obj(search_obj_arg, "PARAGRAPH_ELEMENTS")

    assert mod_search_obj_arg == "textRun"


@pytest.mark.parametrize(
    "example_targeted_text,example_replacement_text",
    [
        ("dummy text 01", "text 01 v1"),
        ("abc", "def"),
        (
            "another test_",
            "another test",
        ),
    ],
)
def test_replace_text(example_targeted_text, example_replacement_text):
    response = utils.replace_text(example_targeted_text, example_replacement_text)

    assert response == {
        "replaceAllText": {
            "containsText": {"text": example_targeted_text, "matchCase": "true"},
            "replaceText": example_replacement_text,
        }
    }


@pytest.mark.parametrize(
    "range_dict,paragraph_style_dict, fields_string",
    [
        (
            {"startIndex": 90, "endIndex": 560},
            {"lineSpacing": 100, "direction": "LEFT_TO_RIGHT"},
            "lineSpacing,direction",
        ),
        (
            {"startIndex": 100, "endIndex": 200},
            {"spaceAbove": {"unit": "PT"}, "spaceBelow": {"unit": "PT"}},
            "spaceAbove, spaceBelow",
        ),
    ],
)
def test_update_paragraph_style(
    mocker, range_dict, paragraph_style_dict, fields_string
):
    mock_validate_range_dict = mocker.patch("fw_gdrive.utils.validate_range_dict")
    mock_validate_range_dict.return_value = range_dict

    mock_populate_fields_string = mocker.patch("fw_gdrive.utils.populate_fields_string")
    mock_populate_fields_string.return_value = fields_string

    actual_response = utils.update_paragraph_style(
        range_dict,
        paragraph_style_dict,
    )

    assert actual_response == {
        "updateParagraphStyle": {
            "range": range_dict,
            "paragraphStyle": paragraph_style_dict,
            "fields": fields_string,
        }
    }


@pytest.mark.parametrize(
    "range_dict,text_style_dict, fields_string",
    [
        (
            {"startIndex": 1, "endIndex": 10},
            {"bold": True, "fontSize": {"magnitude": 20, "unit": "PT"}},
            "bold, fontSize",
        ),
        (
            {"startIndex": 50, "endIndex": 100},
            {"italic": True, "fontSize": {"magnitude": 23, "unit": "PT"}},
            "italic,fontSize",
        ),
    ],
)
def test_update_text_style(mocker, range_dict, text_style_dict, fields_string):
    mock_validate_range_dict = mocker.patch("fw_gdrive.utils.validate_range_dict")
    mock_validate_range_dict.return_value = range_dict

    mock_populate_fields_string = mocker.patch("fw_gdrive.utils.populate_fields_string")
    mock_populate_fields_string.return_value = fields_string

    actual_response = utils.update_text_style(
        text_style_dict,
        range_dict,
    )
    assert actual_response == {
        "updateTextStyle": {
            "range": range_dict,
            "textStyle": text_style_dict,
            "fields": fields_string,
        }
    }


@pytest.mark.parametrize(
    "doc_style_dict, fields_string",
    [
        (
            {"pageNumberStart": 100, "useCustomHeaderFooterMargins": False},
            "pageNumberStart,useCustomHeaderFooterMargins",
        ),
        (
            {"defaultFooterId": "abc", "defaultHeaderId": "efg"},
            "defaultFooterId,defaultHeaderId",
        ),
    ],
)
def test_update_document_style(mocker, doc_style_dict, fields_string):
    mock_populate_fields_string = mocker.patch("fw_gdrive.utils.populate_fields_string")
    mock_populate_fields_string.return_value = fields_string

    actual_response = utils.update_document_style(doc_style_dict)

    assert actual_response == {
        "updateDocumentStyle": {
            "documentStyle": doc_style_dict,
            "fields": fields_string,
        }
    }


def test_insert_page_break():
    actual_response = utils.insert_page_break("20")

    assert actual_response == {
        "insertPageBreak": {
            "location": {"index": 20},
        }
    }


@pytest.mark.parametrize(
    "index, doc_content, format_str",
    [
        (53, "Test {} ABC", ("\n" * 5,)),
        (100, "SAMPLE Text {} For {} Testing", ("\n" * 2, "\t" * 5)),
    ],
)
def test_insert_text(index, doc_content, format_str):
    actual_response = utils.insert_text(index, doc_content, format_str)

    assert actual_response == {
        "insertText": {
            "location": {"index": index},
            "text": doc_content.format(*format_str),
        }
    }


@pytest.mark.parametrize(
    "range_dict, expected_response",
    [
        (
            {"startIndex": 30, "endIndex": 40},
            {"deleteContentRange": {"range": {"startIndex": 30, "endIndex": 40}}},
        ),
    ],
)
def test_delete_content(mocker, range_dict, expected_response):
    mock_validate_range_dict = mocker.patch("fw_gdrive.utils.validate_range_dict")
    mock_validate_range_dict.return_value = range_dict

    actual_response = utils.delete_content(range_dict)

    assert actual_response == expected_response


@pytest.mark.parametrize(
    "field_dict, expected_response",
    [
        ({"test": "abx", "another": "wer", "dummy": "test"}, "test,another,dummy"),
    ],
)
def test_populate_fields_string(field_dict, expected_response):
    actual_response = utils.populate_fields_string(field_dict.keys())

    assert actual_response == expected_response


@pytest.mark.parametrize(
    "range_dict, expected_range_dict",
    [
        ({"startIndex": "23", "endIndex": 56}, {"startIndex": 23, "endIndex": 56}),
        ({"startIndex": 90, "endIndex": 560}, {"startIndex": 90, "endIndex": 560}),
    ],
)
def test_validate_range_dict(range_dict, expected_range_dict):
    actual_range_dict = utils.validate_range_dict(range_dict, "range")

    assert actual_range_dict == expected_range_dict
