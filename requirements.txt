cachetools==5.2.0 ; python_version >= "3.7" and python_version < "4.0"
certifi==2022.6.15 ; python_version >= "3.7" and python_version < "4"
charset-normalizer==2.1.0 ; python_version >= "3.7" and python_version < "4"
google-api-core==2.8.2 ; python_version >= "3.7" and python_version < "4.0"
google-api-python-client==2.52.0 ; python_version >= "3.7" and python_version < "4.0"
google-auth-httplib2==0.1.0 ; python_version >= "3.7" and python_version < "4.0"
google-auth==2.9.0 ; python_version >= "3.7" and python_version < "4.0"
googleapis-common-protos==1.56.3 ; python_version >= "3.7" and python_version < "4.0"
httplib2==0.20.4 ; python_version >= "3.7" and python_version < "4.0"
idna==3.3 ; python_version >= "3.7" and python_version < "4"
oauth2client==4.1.3 ; python_version >= "3.7" and python_version < "4.0"
protobuf==4.21.2 ; python_version >= "3.7" and python_version < "4.0"
pyasn1-modules==0.2.8 ; python_version >= "3.7" and python_version < "4.0"
pyasn1==0.4.8 ; python_version >= "3.7" and python_version < "4.0"
pyparsing==3.0.9 ; python_version >= "3.7" and python_version < "4.0"
requests==2.28.1 ; python_version >= "3.7" and python_version < "4"
rsa==4.8 ; python_version >= "3.7" and python_version < "4"
six==1.16.0 ; python_version >= "3.7" and python_version < "4.0"
uritemplate==4.1.1 ; python_version >= "3.7" and python_version < "4.0"
urllib3==1.26.10 ; python_version >= "3.7" and python_version < "4"
